const vscode = require('vscode'),
      fs = require('fs')

class TestOutputInterpeter {
  constructor(rubyTestRunner, outputPath, errorMessageLines, testErrorStartRegex = null, haltInterpretingRegex = null) {
    this.rubyTestRunner = rubyTestRunner
    this.outputPath = outputPath
    this.errorMessageLines = errorMessageLines
    this.testErrorStartRegex = testErrorStartRegex
    this.haltInterpretingRegex = haltInterpretingRegex
  }

  getFailedTests(testFileRelativePath) {
    let testOutputInterpeter = this

    return new Promise((resolve, reject) => {
      fs.readFile(testOutputInterpeter.outputPath, 'utf8', (err, data) => {
        if(err) {
          reject(err)
          return
        }

        let lines = data.split(/\r?\n/)
        
        let failedTests = []
        let failedTestsErrorLines = []
        let insideTestErrorReport = false
        let testErrorLineRegex = new RegExp(`${testFileRelativePath}:\\d+`)
        let testErrorLinePresent
        let errorStart
        let errorLineNumber
        let testDefinitionLineNumber
        let errorMessage

        let testDefinitions = testOutputInterpeter.getAtomicTests()
  
        for (let i = 0; i < lines.length; i++) {
          let line = lines[i].trim().substr(0, 1000)
  
          if(insideTestErrorReport) {
            testErrorLinePresent = testErrorLineRegex.exec(line)
  
            if(testErrorLinePresent) {
              errorLineNumber = parseInt(line.split(':')[1])
  
              for(let j = testDefinitions.length - 1; j >= 0; j--) {
                if(testDefinitions[j].lineNumber > errorLineNumber) continue
  
                testDefinitionLineNumber = testDefinitions[j].lineNumber
                break
              }
  
              failedTests.push({
                hoverMessage: errorMessage,
                range: new vscode.Range(new vscode.Position(testDefinitionLineNumber - 1, 0), new vscode.Position(testDefinitionLineNumber - 1, line.length - 1)),
                errorLineNumber: errorLineNumber,
                testDefinitionLineNumber: testDefinitionLineNumber
              })
              failedTestsErrorLines.push({
                range: new vscode.Range(new vscode.Position(errorLineNumber - 1, 0), new vscode.Position(errorLineNumber - 1, 500)),
                hoverMessage: errorMessage
              })

              insideTestErrorReport = false
            } else if(testOutputInterpeter.haltInterpretingRegex && testOutputInterpeter.haltInterpretingRegex.exec(line)) break
          } else {
            errorStart = testOutputInterpeter.testErrorStartRegex.exec(line)
            if(!errorStart) continue

            errorMessage = "```"
            for(let j = i + 2; j <= i + 2 + testOutputInterpeter.errorMessageLines; j++) errorMessage += "\n" + lines[j].trim().substr(0, 200).replace(/`/g, "'")
            errorMessage += "\n```"

            insideTestErrorReport = true
          }
        }

        resolve([failedTests, failedTestsErrorLines])
      })
    })
  }

  // get leaves of the test tree structure
  getAtomicTests() {
    let tests = this.getPassedTests([])
    return quickSort(tests)
  }

  getPassedTests(failedTests, targetedTestObject = null) {
    if(failedTests.length != 0 && !failedTests[0].testDefinitionLineNumber) return []
    
    let dfsStack = []
    let passedTests = []
    let independentTrees = []
    let currentElement = null
    let explored
    let testOutputInterpeter = this

    let remainingFailedTests = failedTests.slice()

    if(targetedTestObject) {
      independentTrees = [targetedTestObject]
    } else {
      this.rubyTestRunner.codeLensProvider.testStructures.forEach((node) => {
        if(node.children.length == 0) {
          if(!testOutputInterpeter.testIsFailed(remainingFailedTests, node)) passedTests.push(node)
          return
        }

        independentTrees.push(node)
      })
    }

    for(let treeRoot of independentTrees) {
      dfsStack = [treeRoot]
      explored = new Set()
      explored.add(treeRoot)

      while(dfsStack.length != 0) {
        currentElement = dfsStack.pop()

        if(currentElement.children.length == 0) {
          if(!testOutputInterpeter.testIsFailed(remainingFailedTests, currentElement)) passedTests.push(currentElement)
          continue
        }

        currentElement.children.forEach((child) => {
          if(explored.has(child)) return

          explored.add(child)
          dfsStack.push(child)
        })
      }
    }

    return passedTests
  }

  testIsFailed(remainingFailedTests, testObject) {
    let failedTest

    for(let i = 0; i < remainingFailedTests.length; i++) {
      failedTest = remainingFailedTests[i]
      if(failedTest.testDefinitionLineNumber == testObject.lineNumber) {
        remainingFailedTests.splice(i, 1)
        return true
      }
    }

    return false
  }
}

function quickSort(array) {
  if (array.length == 0) return []

  var left = [], right = [], pivot = array[0]
  for (var i = 1; i < array.length; i++) {
      if(array[i].lineNumber < pivot.lineNumber) left.push(array[i])
      else right.push(array[i])
  }

  return quickSort(left).concat(pivot, quickSort(right))
}

module.exports = {
  TestOutputInterpeter
}
