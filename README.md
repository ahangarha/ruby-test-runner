# Ruby Test Runner

![Icon](images/icon.png)

Run Ruby tests with one click (or key binding)!

This extension is mainly targeted at Ruby on Rails projects. Though it is highly customizable through extension settings, where you may provide custom commands for running different kinds of tests.

Update: Live test results in the editor!
![Live Test Results](images/live-tests-overview.gif)

## Features

It is possible to run Ruby tests with one click.

![Overview](images/overview.gif)

This extension makes use of CodeLens to let you run single tests within a test file with just a click.

![CodeLens](images/code-lens.png)

There are three Status Bar Buttons:
* Unit tests - runs all unit tests
* System tests - runs all system tests
* Run test - runs the currently open test file. When there is a selection in the test file, the starting line of the selection will be passed to the test runner

![Buttons](images/buttons.png)

There is a key binding for running the currently open test file with `Ctrl + Shift + T`. It works similarily to the 'Run test' Status Bar Button. When there is a selection of text in the current test file, the starting line of this selection will be passed to the test runner after a semicolon. (eg. `rails t /Users/some_user/some_test.rb:27`)

![Key bindings](images/keybindings.gif)

Another keybinding is `Ctrl + Shift + a` which runs all unit tests.

## Extension Settings

| Setting name | Description | Default value |
| ------------ | ----------- | ------------- |
| `rubyTestRunner.unitTestCommand` | Provide a custom command for running unit tests | `"bundle exec rails t"` |
| `rubyTestRunner.unitTestCommandRails4` | Provide a custom command for running unit tests in Rails < 5 | `"bundle exec rake test TEST="` |
| `rubyTestRunner.specTestCommand` | Provide a custom command for running spec tests | `"bundle exec rspec"` |
| `rubyTestRunner.systemTestCommand` | Provide a custom command for running system tests | `"bundle exec rails test:system"` |
| `rubyTestRunner.systemTestCommandWithoutRails` | Provide a custom command for running system tests in projects without Rails system tests | `"bundle exec rspec"` |
| `rubyTestRunner.recognizedTestMethodNames` | Method names which the extension will recognize as test definitions, seperated with a comma | `"should, scenario, context, describe, it, test"` |
| `rubyTestRunner.runTestCodeLensEnabled` | Choose whether the dynamic 'Run test' code lenses in the source code view shall be generated | `true` |
| `rubyTestRunner.runAllUnitTestsButtonEnabled` | Choose whether the 'Unit tests' Status Bar Button should be shown | `true` |
| `rubyTestRunner.runAllSystemTestsButtonEnabled` | Choose whether the 'System tests' Status Bar Button should be shown | `true` |
| `rubyTestRunner.runTestButtonEnabled` | Choose whether the 'Run test file' Status Bar Button should be shown | `false` |
| `rubyTestRunner.openTestsInIterm` | Open tests in iTerm instead of the VSCode terminal | `false` |

## Keybindings

You can edit the keybindings by pasting this in Code -> Preferences -> Keyboard Shortcuts -> keybindings.json:

```json
{
  "key": "ctrl+shit+t",
  "command": "ruby-test-runner.run-current-test-file",
  "when": "editorTextFocus"
},
{
  "key": "ctrl+shift+a",
  "command": "ruby-test-runner.run-all-unit-tests",
  "when": "editorTextFocus"
}
```
